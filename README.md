# Notes for Code Ta Bai: TDD

**NOTE: I do not recommend that you code along with me when I do my lecture
(Medyo lisod man gud maminaw if magtuplok2 pa mo sa inyong mga laptop).  
If you want to try it out beforehand though this guide will hopefully help.  
If you are having any difficulties please feel free to send me a PM on facebook.**

## Groovy

- I'm going to be using groovy because I want to.  
- [Groovy Installation](http://groovy-lang.org/install.html)  
- TLDR. Download [groovy](https://bintray.com/artifact/download/groovy/maven/apache-groovy-binary-2.4.6.zip). Download [java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)  
- **NOTE:** You can download any version of java above version 5(i.e. 1.6 and above)
- Set the GROOVY_HOME and JAVA_HOME environment variables.  
- Add GROOVY_HOME\bin and JAVA_HOME\bin to your PATH.

## Gradle

- You're going to need Gradle to handle the dependencies.  
- Download gradle from [here](http://www.gradle.org/downloads)  
- Unpack it somewhere then add a GRADLE_HOME variable to your path(Just like the Groovy installation).  
- Also add GRADLE_HOME\bin to your PATH.

## Project Setup

- Create a `build.gradle` file. This file will tell gradle the jars that you need for this project.

```groovy
apply plugin: 'groovy'

repositories {
    jcenter()
}

dependencies {
    // The core spock framework jar
    testCompile(
            'org.spockframework:spock-core:1.0-groovy-2.4'
    )
    // This is used for report generation. You can see the test report in the file generated.
    testCompile( 'com.athaydes:spock-reports:1.2.10' ) {
        transitive = false // this avoids affecting your version of Groovy/Spock
    }

    // if you don't already have slf4j-api and an implementation of it in the classpath, add this!
    testCompile 'org.slf4j:slf4j-api:1.7.13'
    testCompile 'org.slf4j:slf4j-simple:1.7.13'

}

// This is for outputting test logs to the console
tasks.withType(Test) {
    testLogging {
        exceptionFormat "full"
    }
}
```

- Put this file in the root of your project folder. If you're project is located in C:\Desktop\tdd\
then you need to save this file as C:\Desktop\tdd\build.gradle.  
- Try to run `gradle clean test` to see if it works.

## Unit Testing

- Create a file named `FooTest.groovy` in `PROJECT_DIR\src\test\groovy\`

```groovy
import spock.lang.Specification

class FooTest extends Specification {

    def "simple addition test"() {
        given:
            def math = new Math()
        
        when:
            def sum = math.add(3, 4)
            
        then:
            sum == 7
    }
    
    def "simple subtraction test"() {
        given:
            def math = new Math()
            
        when:
            def difference = math.subtract(5, 2)
            
        then:
            difference == 3
    }
    
    class Math {
        
        def add(a, b) {
            return a + b
        }
        
        def subtract(a, b) {
            return a - b
        }
    }
}
```

- Run `gradle test` to check if everything is working okay.
- Try to change the values and see if the tests fail.
- Look for spock docs. Play around with it.
