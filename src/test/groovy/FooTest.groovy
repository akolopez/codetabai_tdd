import spock.lang.Specification

class FooTest extends Specification {

	Math math

	def setup() {
		math = new Math()
	}

    def "simple addition test"() {
        when:
            def sum = math.add(3, 4)
            
        then:
            sum == 7
    }
    
    def "simple subtraction test"() {
        when:
            def difference = math.subtract(5, 2)
            
        then:
            difference == 3
    }

    def "subtraction with negative result."() {
    	when:
    		def difference = math.subtract(2, 5)

    	then:
    		difference == -3
    }

    def 'dividing by zero.'() {
    	when:
    		def quotient = math.divide(4, 0)

    	then:
    		thrown(ArithmeticException)
    }
    
    class Math {
        
        def add(a, b) {
            return a + b
        }
        
        def subtract(a, b) {
            return a - b
        }

        def divide(a, b) {
        	return a / b
        }
    }
}