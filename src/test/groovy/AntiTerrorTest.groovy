import spock.lang.Specification

class AntiTerrorTest extends Specification {

	AntiTerror at
	ThirdPartyHelper thirdPartyHelper

	def setup() {
		at = new AntiTerror()
		thirdPartyHelper = Mock()
		at.thirdPartyHelper = thirdPartyHelper
	}

	void 'isTerrorist should return FALSE if name does not start with R.'() {
		when:
			def isTerrorist = at.isTerrorist(name, '')

		then:
			isTerrorist == false

		where:
			name << ['OCHELEE', 'FAYE', 'ARVIN']
	}

	void 'isTerrorist should return TRUE if name starts with R'() {
		when:
			def isTerrorist = at.isTerrorist(name, '')

		then:
			isTerrorist == true

		where:
			name << ['rOCHELEE', 'ROLAND', 'ROLDAN']
	}

	void 'isTerrorist should return TRUE if search history is suspicious.'() {
		given:
			thirdPartyHelper.searchHistoryIsSuspicious() >> true

		when:
			def isTerrorist = at.isTerrorist('Baron Geisler', '')

		then:
			isTerrorist == true
	}

	void 'isTerrorist should return FALSE if search history is not suspicious.'() {
		given:
			thirdPartyHelper.searchHistoryIsSuspicious() >> false

		when:
			def isTerrorist = at.isTerrorist('Baron Geisler', '')

		then:
			isTerrorist == false
	}

	void 'isTerrorist should return TRUE if email is suspicious.'() {
		given:
			thirdPartyHelper.emailIsSuspicious(email) >> true

		when:
			def isTerrorist = at.isTerrorist('xxx', email)

		then:
			isTerrorist == true

		where:
			email << [
				'bombasaairport@yahoo.com',
				'isis@syria.com',
				'alohasnackbar@binay.com'
			]
	}

	void 'isTerrorist should return FALSE if email is not suspicious.'() {
		given:
			thirdPartyHelper.emailIsSuspicious(email) >> false

		when:
			def isTerrorist = at.isTerrorist('xxx', email)

		then:
			isTerrorist == false

		where:
			email << [
				'bombasaairport@yahoo.com',
				'isis@syria.com',
				'alohasnackbar@binay.com'
			]
	}

	void 'doStuff should make the phone ring.'() {
		when:
			at.doStuff()

		then:
			1 * thirdPartyHelper.ringPhone()
	}

	class AntiTerror {

		def thirdPartyHelper

		public boolean isTerrorist(String name, String email) {
			boolean isTerrorist = false
			if (name.toUpperCase().startsWith('R') ||
			    thirdPartyHelper.searchHistoryIsSuspicious() ||
			    thirdPartyHelper.emailIsSuspicious(email)) {
				isTerrorist =  true
			}

			return isTerrorist
		}

		public void doStuff() {
			thirdPartyHelper.ringPhone()
		}
	}	

	class ThirdPartyHelper {

		public boolean searchHistoryIsSuspicious() {

		}

		public boolean emailIsSuspicious(String emailaddress) {

		}

		public void ringPhone() {

		}
	}
}